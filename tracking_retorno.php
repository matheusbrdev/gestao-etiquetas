<?php
require_once 'checkout_session.php';

include 'apiVTex.php';
include 'db_querys.php';

$db = new DbQuerys();
$api = new apiVTex();

$array = $_POST['check'];
$dateFormat2 = '%A, %d de %B de %Y';

?>
<!DOCTYPE html>
<html lang="pt-br" class="default-style">
<head>
    <title>Retorno tracking</title>
    <?php require_once './assets/layout/head-config.html' ?>
</head>

<body id="bodyIndex" class="background-color">
<?php require_once './assets/layout/header.php'; ?>
<!--Container-->
<div class="container py-3">
    <div class="d-flex justify-content-center my-5">
        <h1 class="display-3 font-weight-bold font-color">
            Retorno Tracking
        </h1>
    </div>

    <!--row-->
    <form action="teste.php" method="POST">
        <div class="row">
            <!--col-md-12-->
            <div class="col-md-12">
                <div class="row">
                    <div class="col-6 d-flex justify-content-start">
                        <h1 class="display-4">Itens:</h1>
                    </div>
                </div>
                <table class="table">
                    <thead class="thead-primary">
                        <tr>
							<th scope="col">Orçamento</th>
							<th scope="col">Pedido VTex</th>
                            <th scope="col">Cod. Rastreio</th>
                            <th scope="col">Retorno VTex</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php
						if(empty($array))
							echo "<h3>Nenhum pedido foi selecionado</h3>";
						else{
							foreach($array as $orcamento) {
								$return = $db->getDadosTracking($orcamento);

								$retornoApi = $api->enviaTracking(
									$return['cd_rastreio'], // $cd_rastreio
									$return['courier'], // $courier
									$return['dispatchedDate']->format(DateTime::ATOM), // $dataEnvio
									$return['cd_orc'], // $cdOrc
									$return['codigo_ecommerce'], // $codigoEcommerce
									$return['invoice'] // $invoice
								); ?>
								<tr>
								<td><?php echo $return['cd_orc']; ?></td>
								<td><?php echo $return['codigo_ecommerce']; ?></td>
								<td><?php echo $return['cd_rastreio']; ?></td>
								<td><?php if($retornoApi == '200'){ 
											echo "Enviado com Sucesso";
											$atualiza = $db->updateRetVTex($orcamento,$retornoApi);
										}else{
											$erro = 'Erro: '.$retornoApi->error->message;
											echo $erro;
											if($erro = 'Erro: invoiceNumber')
												$erro = '404 - Pedido cancelado';
											$atualiza = $db->updateRetVTex($orcamento,$erro);
										}
									?>
								</td>
								</tr><?php
							
								
							}
						}
					?>	
                    </tbody>
                </table>
            </div>
            <!--col-md-12-->
        </div>
    </form>
    <!-- // row-->
</div>
<!-- // Container-->

<?php require_once './assets/layout/footer.html'; ?>
<?php require_once './assets/layout/scripts.html'; ?>
</body>
</html>
