<?php 

require_once 'checkout_session.php';
include 'db_querys.php';

$conect = new DbConnection();
$dbQuerys = new DbQuerys;
$transpAberta = $dbQuerys->getTranspAbertas()['qtd'];
//$transpAberta = 0; // Se $transpAberta == 0, então botão fica inativo

$query = "exec p_log_ecom_lst_lote";        
$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());

$usuario = $_SESSION['cd_usu'];

if (isset($_SESSION['cd_transportadora']) and isset($_SESSION['id_plp'])) {
    $_SESSION['cd_transportadora'] = '';
    $_SESSION['id_plp'] = '';
}

if (isset($_POST['id_plp']))  {

    $retorno = $dbQuerys->fechaLote($_POST['id_plp']);

    if ($retorno == 'OK') {
        $_SESSION['retorno'] = 'Lote fechado com sucesso !';

    } else {
        $_SESSION['retorno'] = $retorno;
    }
}

?>
<!DOCTYPE html>
<html lang="pt-br" class="default-style">
  <head>
    <title>Lista de pendências</title>

    <?php require_once './assets/layout/head-config.html' ?>
    <style>
        body#bodyIndex{
            z-index: 1;
        }

        div#modalLoad {
            position: fixed;
            top: 0;
            z-index: 11111;
            background: rgba(10, 10, 14, .5);
            height: 100vh;
            width: 100%;
        }
    </style>

    <!--Datatables-->
    <link
      rel="stylesheet"
      href="assets/css/jquery.dataTables.min.css"
    >
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>
    <script src="./assets/vendor/js/bootstrap.js" type="text/javascript"></script>
    <script src="./assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        $.noConflict();
        $('#dataTables').DataTable();
      } );
    </script>

  </head>

  <body id="bodyIndex" class="background-color">
    <?php require_once './assets/layout/header.php'; ?>
    <!--Container-->
    <div class="container py-3">
        <?php if ($_SESSION['retorno']){
          echo "<p class='alert alert-warning font-weight-bold text-center' role='alert'>{$_SESSION['retorno']}</p>";
          $_SESSION['retorno'] = '';
        } 
        ?>  
      <div class="d-flex justify-content-center my-5">
        <h1 class="display-3 font-weight-bold font-color">
          Lotes em Aberto
        </h1>
      </div>

      <!--row-->
      <div class="row">
        <!--col-md-12-->
        <div class="col-md-12">
          <div class="row">
            <div class="col-6 d-flex justify-content-start">
              <h1 class="display-4">Lotes:</h1>
            </div>
            <div id="div-button" class="col-6 d-flex justify-content-end align-items-start">
              <p id="message-button" class="alert alert-warning d-none">Não existe transportadora disponível para abertura de lote</p>
              <a buttonAction href="select_company.php" class="btn btn-primary   <?php if($transpAberta <= 0){echo 'button-create';} ?> ml-auto">
                Criar lote
              </a>
            </div>
          </div>
          <table class="table" id="dataTables">
            <thead class="thead-primary">
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Usuário</th>
                <th scope="col">Abertura lote</th>
                <th scope="col">Fechamento</th>
                <th scope="col">Transportadora</th>
                <th scope="col">Ações</th>
              </tr>
            </thead>
            <tbody><?php 
                if($conn){
                    $abreLote = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));    
                    while($result = sqlsrv_fetch_array($abreLote)){
                        if($result['cd_plp'] == null){ ?>
                            <tr>
                                <td><?php echo $result['id_plp']; ?></td>
                                <td><?php echo $result['usu_conf']; ?></td>
                                <td><?php echo date_format($result['dthr_conf'],"d/m/Y H:i"); ?></td>
                                <td><?php if($result['dthr_fech'] == null)
                                        echo "---";
                                    else
                                        echo date_format($result['dthr_fech'],"d/m/Y H:i"); ?>
                                </td>
                                <td><?php echo $result['nm_transportadora']; ?></td>
                                <td align="center">
                                    <?php if($result['dthr_fech'] == null){ ?>
                                      <!--form conferir -->
                                    <form action="launch_lote.php" method="post" class="d-inline">
                                      <input type="hidden" name="id_plp"
                                      value="<?= $result['id_plp'] ?>">

                                      <input type="hidden" name="cd_transportadora"
                                      value="<?= $result['cd_transportadora'] ?>">
                                     <button class='btn btn-outline-info'>Conferir</button>  
                                    </form>
                                    <!-- / form conferir -->
                                    <button type='button' value='<?php echo $result['id_plp']; ?>' class='btn btn-outline-success' data-toggle="modal" data-target="#modalFechar<?php echo $result['id_plp']; ?>">Fechar</button>
                                    <?php }else{ ?>
                                    <button type='button' value='<?php echo $result['id_plp']; ?>' class='btn btn-outline-danger' data-toggle="modal" data-target="#modalEnviar<?php echo $result['id_plp']; ?>">Enviar Lote</button><?php } ?>
                                </td>
                            </tr>
                                <!-- MODAL Fechar-->
                                <div class="modal fade" id="modalFechar<?php echo $result['id_plp']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Confirmação de fechamento de lote</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
										<?php echo "Usuário: ".$_SESSION['cd_usu']."<br>"; ?>
                                        Deseja realmente fechar o lote <?php echo $result['id_plp']; ?>?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
                                        <!--form fechar -->
                                        <form action="#" method="post">
                                          <input type="hidden" name="id_plp" value="<?= $result['id_plp'] ?>">
                                          <button class="btn btn-primary">Fechar lote</button>
                                        </form>
                                        <!-- / form fechar -->
                                      </div>
                                    </div>
                                  </div>
                                </div>    
                                <!-- MODAL -->  

                                <!-- MODAL Enviar Lote-->
                                <div modal class="modal fade" id="modalEnviar<?php echo $result['id_plp']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Confirmação de envio de lote</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
										<?php echo "Usuário: ".$_SESSION['cd_usu']."<br>"; ?>
                                        Deseja realmente enviar o lote <?php echo $result['id_plp']; ?>?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
										<a id="buttonConfirm" onclick="hideModal()" class="btn btn-primary" href="<?php echo 'comunica_correios.php?plp='.$result['id_plp']; ?>">
                                        	Enviar lote
										</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>    
                                <!-- MODAL --> 
                
                        <?php } //Fecha IF result CD_PLP 
                    } // Fecha while fetch_array
                } // Fecha IF $conn
                else{
                    return die (print_r(sqlsrv_errors(),true));
                }
                ?>
              
            </tbody>
          </table>
        </div>
        <!--col-md-12-->
      </div>
      <!-- // row-->
    </div>
    <!-- // Container-->
    <div id="modalLoad" class="d-none">
        <img src="assets/img/loading.gif" alt="loading" width="100">
        <h2 class="display-6 text-white">Comunicando com os Correios...</h2>
    </div>
      
    <script>
      //Menssagem no botão 'criar lote'
      const buttonCreate = document.querySelector('a[buttonAction]');
      const buttonCreateClasses = document.querySelector('a[buttonAction]').classList;
      const menssage = document.querySelector('p#message-button');

      //actions

      //Menssagem no botão 'crair lote'
      if(buttonCreateClasses.contains('button-create')) {
        buttonCreate.addEventListener('click', function(event){
            event.preventDefault();
        })

        buttonCreate.addEventListener("mouseover", function( event ) {
          menssage.classList.remove('d-none')

          buttonCreate.addEventListener("mouseout", function( event ) {
          menssage.classList.add('d-none')
        })
        })
      }

      function hideModal() {
          const divModal = document.querySelector('div#modalLoad').classList;

          divModal.add('d-flex');
          divModal.add('flex-column');
          divModal.add('justify-content-center');
          divModal.add('align-items-center');
          divModal.remove('d-none');

          if(divModal.contains('d-flex')) {
              const body = document.querySelector('#bodyIndex');

              body.style.overflow = 'hidden';
          }

          return 0;
      }
    </script>
      
    <?php require_once './assets/layout/footer.html'; ?>    
    <?php require_once './assets/layout/scripts.html'; ?>
  </body>
</html>
