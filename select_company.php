<?php 
session_start();
require_once 'checkout_session.php';

include 'db_querys.php';

$conect = new DbConnection();
$dbQuerys = new DbQuerys();

$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());

$qtdTransp = $dbQuerys->getTranspAbertas();

if($qtdTransp > 0){
    $query = "exec p_log_ecom_lst_transp";
    
    $query = sqlsrv_query($conn,$query) or die(print_r(sqlsrv_errors(), true));
}


if (isset($_POST['cd_usu']) and isset($_POST['cd_transportadora'])) {

    $retorno = $dbQuerys->abreLote($_POST['cd_usu'], $_POST['cd_transportadora']);

    if ($retorno == 'OK') {
        $_SESSION['retorno'] = 'Lote criado com sucesso !';

    } else {
        $_SESSION['retorno'] = $retorno;
    }

    header('Location: index.php');
}

?>


<!DOCTYPE html>
<html lang="pt-br" class="default-style">
  <head>
    <title>Transportadora</title> 
    <link rel="stylesheet" href="assets/vendor/css/pages/authentication.css" />
    <!-- head-config -->
    <?php require_once './assets/layout/head-config.html' ?>
  </head>
    
    <body class="background-color">
    <?php require_once './assets/layout/header.php'; ?>
    <!--Container-->
    <div class="container py-3">
      <div class="d-flex justify-content-center my-5">
        <h1 class="display-3 font-weight-bold font-color">
          Transportadoras disponíveis
        </h1>
      </div>

      <!--row-->
      <div class="row">
      <table class="table">
        <thead class="thead-primary">
          <tr>
            <th scope='col'>Código</th>
            <th scope='col'>Transportadora</th>
            <th scope='col'>Ação</th>
          </tr>
        </thead>
        <tbody><?php 
            while($result = sqlsrv_fetch_array($query)){ ?>
                <tr>
                    <td><?php echo $result['cd_transportadora']; ?></td>
                    <td><?php echo $result['nm_transportadora']; ?></td>
                    <td>
                        <button 
                                type='button' 
                                class='btn btn-outline-success view_data'
                                id='<?php echo $result['cd_emp_entrega']; ?>' 
                                data-toggle="modal" 
                                data-target="#modalAbrir<?php echo $result['cd_transportadora']; ?>">
                            Abrir Lote
                        </button>
                    </td>
                </tr>
                <!-- MODAL Abrir-->
                <div class="modal fade" id="modalAbrir<?php echo $result['cd_transportadora']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Confirmação de fechamento de lote</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        Deseja realmente abrir novo lote?<br>
                        <?php echo "Usuário: ".$_SESSION['cd_usu']; ?>
                        <?php echo "<br>Transportadora: ".$result['cd_transportadora']." - ".$result['nm_transportadora']; ?>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
                        <form method="post">
                          <!-- input hidden cd_usu-->
                          <input type="hidden" name="cd_usu"
                          value="<?= $_SESSION['cd_usu']; ?>">
                          <!-- / input hidden cd_usu-->

                          <!-- input hidden cd_transportadora-->
                          <input type="hidden" name="cd_transportadora"
                          value="<?= $result['cd_transportadora']; ?>">
                          <!-- / input hidden cd_transportadora-->

                          <button class="btn btn-primary">Sim</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>    
                <!-- MODAL -->  
            <?php } //Fecha While  ?>

        </tbody>
      </table>
    </div>
        <!--col-md-12-->
      </div>
      <!-- // row-->
    </div>
    <!-- // Container-->

    


      
    <?php require_once './assets/layout/footer.html'; ?>    
    <?php require_once './assets/layout/scripts.html'; ?>
  </body>

</html>