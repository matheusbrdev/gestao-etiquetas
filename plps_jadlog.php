<?php 

require_once 'checkout_session.php';
include 'db_querys.php';

$conect = new DbConnection();
$dbQuerys = new DbQuerys;
$transpAberta = $dbQuerys->getTranspAbertas()['qtd'];
//$transpAberta = 0; // Se $transpAberta == 0, então botão fica inativo

$query = "exec p_log_ecom_lst_lote";        
$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());

$usuario = $_SESSION['cd_usu'];

if (isset($_POST['id_plp']))  {

    $retorno = $dbQuerys->fechaLote($_POST['id_plp']);

    if ($retorno == 'OK') {
        $_SESSION['retorno'] = 'Lote fechado com sucesso !';

    } else {
        $_SESSION['retorno'] = $retorno;
    }
}

?>
<!DOCTYPE html>
<html lang="pt-br" class="default-style">
  <head>
    <title>PLPs Jadlog</title>

    <?php require_once './assets/layout/head-config.html' ?>

    <!--Datatables-->
    <link
      rel="stylesheet"
      href="assets/css/jquery.dataTables.min.css"
    >
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>
    <script src="./assets/vendor/js/bootstrap.js" type="text/javascript"></script>
    <script src="./assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        $.noConflict();
        $('#dataTables').DataTable();
      } );
    </script>
  </head>

  <body class="background-color">
    <?php require_once './assets/layout/header.php'; ?>
    <!--Container-->
    <div class="container py-3">
        <?php if ($_SESSION['retorno']){
          echo "<p class='alert alert-warning font-weight-bold text-center' role='alert'>{$_SESSION['retorno']}</p>";
          $_SESSION['retorno'] = '';
        } 
        ?>  
      <div class="d-flex justify-content-center my-5">
        <h1 class="display-3 font-weight-bold font-color">
          PLPs Jadlog
        </h1>
      </div>

      <!--row-->
      <div class="row">
        <!--col-md-12-->
        <div class="col-md-12">
          <div class="row">
            <div class="col-6 d-flex justify-content-start">
              <h1 class="display-4">Lotes:</h1>
            </div>
          </div>
          <table class="table" id="dataTables">
            <thead class="thead-primary">
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Usuário</th>
                <th scope="col">Abertura lote</th>
                <th scope="col">Fechamento</th>
                <th scope="col">Transportadora</th>
                <th scope="col">Ações</th>
              </tr>
            </thead>
            <tbody><?php 
                if($conn){
                    $abreLote = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));    
                    while($result = sqlsrv_fetch_array($abreLote)){
                        if(($result['cd_plp'] != null) and ($result['cd_transportadora'] == 17)){ ?>
                            <tr>
                                <td><?php echo $result['id_plp']; ?></td>
                                <td><?php echo $result['usu_conf']; ?></td>
                                <td><?php echo date_format($result['dthr_conf'],"d/m/Y H:i"); ?></td>
                                <td><?php if($result['dthr_fech'] == null)
                                        echo "---";
                                    else
                                        echo date_format($result['dthr_fech'],"d/m/Y H:i"); ?>
                                </td>
                                <td><?php echo $result['nm_transportadora']; ?></td>
                                <td align="center">
									<a href="<?php echo 'fpdf/pdf_jadlog.php?plp='.$result['id_plp']; ?>" target="_blank">
                                    <button type='button' value='<?php echo $result['id_plp']; ?>' class='btn btn-outline-danger'>Imprimir PLP</button>
									</a>
                                </td>
                            </tr>
                
                        <?php } //Fecha IF result CD_PLP 
                    } // Fecha while fetch_array
                } // Fecha IF $conn
                else{
                    return die (print_r(sqlsrv_errors(),true));
                }
                ?>
              
            </tbody>
          </table>
        </div>
        <!--col-md-12-->
      </div>
      <!-- // row-->
    </div>
    <!-- // Container-->
      
      
    <?php require_once './assets/layout/footer.html'; ?>    
    <?php require_once './assets/layout/scripts.html'; ?>
  </body>
</html>
