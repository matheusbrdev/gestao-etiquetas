<?php
session_start();
error_reporting(~E_ALL);
setlocale(LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");
require_once 'db_querys.php';

if ($_SESSION['nm_usu']) {
    header('Location: index.php');
}

class UserValidator extends DbQuerys
{
    protected function isSetUser()
    {
        $isSetUser = isset($_POST['user']);
        $isSetpassword = isset($_POST['password']);

        return $isSetUser and $isSetpassword;
    }

    public function userValidatorLogin()
    {
        if (self::isSetUser()) {
            $user = $_POST['user'];
            $password = $_POST['password'];
            // $dbQuerys = new DbQuerys();
            $returnDbValidation = self::autenticaUsuario($user, $password);

            if ($returnDbValidation['status'] == 0) {
                $_SESSION['errors'] = null;
                $_SESSION['nm_usu'] = $returnDbValidation['nm_usu'];
                $_SESSION['cd_usu'] = $returnDbValidation['cd_usu'];
                // $exp = strtotime('7 hours');

                // setcookie('nm_usu', $returnDbValidation['nm_usu'], $exp);
                // setcookie('cd_usu', $returnDbValidation['cd_usu'], $exp);
                header('Location: index.php');
            } else {
				      $_SESSION['errors'] = [$returnDbValidation['retorno']];

            // echo $returnDbValidation['status'] . "<br>";
            // echo $returnDbValidation['cd_usu'] . "<br>";
            // echo utf8_encode($returnDbValidation['nm_usu'] . "<br>");
            // echo $returnDbValidation['cd_filial'] . "<br>";
            }
        }
    }
}

$userValidator = new UserValidator;
$userValidator->userValidatorLogin();
?>
<!DOCTYPE html>
<html lang="pt-br" class="default-style">
  <head>
    <title>Login - Checkout etiquetas</title> 
    <!-- Page -->
    <link rel="stylesheet" href="assets/vendor/css/pages/authentication.css" />

    <!-- head-config -->
    <?php require_once './assets/layout/head-config.html' ?>
  </head>

  <body>
    <!-- Content -->
    <div class="authentication-wrapper authentication-1 px-4">
      <div class="authentication-inner py-5">
        <!-- Logo -->
        <div class="d-flex justify-content-center align-items-center">
          <div class="ui-w-60">
            <div class="d-flex justify-content-center w-100 position-relative">
              <img
                id="logoIndiana"
                src="assets/img/logo-farmaciaindiana.png"
                alt="logo"
              />
            </div>
          </div>
        </div>
        <!-- / Logo -->
    		<div class="content">
    			<div class="row">
    				<div class="col-12 d-flex justify-content-center align-items-center">
    					<?php if ($_SESSION): ?>
    						<?php foreach($_SESSION['errors'] as $error): ?>
    							<p class="alert-user"><?= utf8_encode($error) ?></p>
    						<?php endforeach ?>
    					<?php $_SESSION['errors'] = ''; endif?>
    				</div>
    			</div>
    		</div>

        <!-- Form -->
        <form class="my-1" action="login.php" method="post">
          <div class="form-group">
            <label class="form-label">Usuário</label>
            <input type="text" name="user" class="form-control" />
          </div>

          <div class="form-group">
            <label class="form-label">Senha</label>
            <input type="password" name="password" class="form-control" />
          </div>

          <div class="mt-3">
            <button class="btn btn-primary">Entrar</button>
          </div>
        </form>
        <!-- / Form -->
      </div>
    </div>
    <!-- / Content -->
  </body>
</html>
