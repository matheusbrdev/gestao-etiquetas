<?php
require_once '../checkout_session.php';
require('fpdf.php');
include '../db_querys.php';

$plp = $_GET['plp'];
$conect = new DbConnection();

$dbQuerys = new DbQuerys;

$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());
$conn2 = sqlsrv_connect($conect->server, $conect->connectionIndianaProducao());

// Código da PLP
$cd_plp = $dbQuerys->getCdPlpCorreios($plp);

if(empty($cd_plp) or ($cd_plp == '')){
	class PDF extends FPDF{
	// Page header
	function Header(){
		// Logo
		$this->Image('correios.jpg',10,4,30);
		// Arial bold 15
		$this->SetFont('Arial','B',30);
		// Move to the right
		$this->Cell(80);
		// Title
		$this->Cell(30,10,'Correios',0,0,'C');
		// Line break
		$this->Ln(15);
		}

	// Page footer
	function Footer(){
		// Position at 1.5 cm from bottom
		$this->SetY(-15);
		// Arial italic 8
		$this->SetFont('Arial','I',8);
		// Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		}
	}
	$pdf = new PDF();
	$pdf->AddPage(); 
	$pdf->SetFont('Arial', 'B', 20);/*Seta fonte: 1 -> Fonte;
	2 -> tipo (B ou I)
	3 -> size*/

	// Título
	$pdf->Cell(190, 20, utf8_decode("PLP Inválida"), 1, 1, 'C');
	$pdf->Ln(3); //linhas de distância

}else{
	
// Quantidade de objetos postais
$query = "exec p_log_ecom_lst_pend @so_pend = 2,@plp = '".$plp."'";
$query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));

$totalObjetos = 0;

while($count = sqlsrv_fetch_array($query)){
	$totalObjetos++;
}

// Itens da PLP (exibição no while)
$query = "exec p_log_ecom_lst_pend @so_pend = 2,@plp = '".$plp."'";
$query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));

// Detalhes da etiqueta e cliente por item da PLP
$queryDetalhes = "select
	a.cd_filial
	,a.ENT_CEP as cep
	,c.nf_nf as nf
	,d.RZ_CLI as destinatario
from tele_orc a
join tele_orc_est_ped_vd b
	on a.cd_emp = b.cd_emp
	and a.cd_filial = b.cd_filial
	and a.cd_orc = b.cd_orc
join est_nf_sai c
	on b.cd_emp = c.cd_emp
	and b.CD_FILIAL = c.CD_FILIAL
	and b.CD_PED = c.CD_PED
join rc_cli d
	on a.cd_cli = d.cd_cli
where a.cd_orc = ";

class PDF extends FPDF
{
	// Page header
	function Header()
	{
		// Logo
		$this->Image('correios.jpg',10,4,30);
		// Arial bold 15
		$this->SetFont('Arial','B',30);
		// Move to the right
		$this->Cell(80);
		// Title
		$this->Cell(30,10,'Correios',0,0,'C');
		// Line break
		$this->Ln(15);
	}

	// Page footer
	function Footer()
	{
		// Position at 1.5 cm from bottom
		$this->SetY(-15);
		// Arial italic 8
		$this->SetFont('Arial','I',8);
		// Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
}




// Instanciation of inherited class
$pdf = new PDF();
$pdf->AddPage(); 
$pdf->SetFont('Arial', 'B', 20);/*Seta fonte: 1 -> Fonte;
2 -> tipo (B ou I)
3 -> size*/

// Título
$pdf->Cell(190, 20, utf8_decode("PRÉ LISTA DE POSTAGEM - PLP"), 1, 1, 'C');
$pdf->Ln(3); //linhas de distância
/*
1 -> largura 
2 -> altura
3 -> conteúdo
4 -> border(0 ou 1)
5 -> quebra de linha ?(0 ou 1)
6 -> justificação
*/

/*-----------------------Assinatura---------------------*/
$pdf->SetFont('Arial', 'B', 13);
$pdf->Cell(95, 10, utf8_decode("Usuário: ".$_SESSION['cd_usu']." - ".$_SESSION['nm_usu']), 1, 0, 'C');
$pdf->Cell(95, 10, "Data entrega: ".date("d/m/y"), 1, 1, 'L');
$pdf->Ln(15); //linhas de distância
$pdf->Cell(190, 10, "__________________________", 0, 1, 'C');
$pdf->Ln(1); //linhas de distância
$pdf->Cell(190, 1, "Ass. Recebimento Correios",0, 1, 'C');

/*-----------------------Assinatura---------------------*/


/*---------------Cabeçalho da tabela----------------------------*/
$pdf->Ln(10);
$pdf->SetFont('Arial', 'B', 12);

$pdf->Cell(
	190,
	10,
	utf8_decode(" Total de Objetos: $totalObjetos
	                PLP Indiana: $plp
	                PLP Correios: $cd_plp"),
	1,
	1,
	'L'
);
/*---------------//Cabeçalho da tabela----------------------------*/

/*---------------Objetos da plp----------------------------*/

while($etiquetas = sqlsrv_fetch_array($query)){
	$detalhes = $queryDetalhes."".$etiquetas['cd_orc'];
	$detalhes = sqlsrv_query($conn2, $detalhes) or die(print_r(sqlsrv_errors(), true));
	
	while($dados = sqlsrv_fetch_array($detalhes)){
		$pdf->Ln(2);
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(190, 19, '',1,0,'L');

		$pdf->Ln(1);
		$pdf->Cell(190, 7, "Cod. Etiqueta: ".$etiquetas['cd_rastreio']."       CEP:".$dados['cep']."         Filial: ".$dados['cd_filial']."       NF: ".$dados['nf']."", 0, 0, 'L');

		$pdf->Ln(5);
		$pdf->Cell(190, 7, utf8_decode("Orçamento: ".$etiquetas['cd_orc']."       Ped. VTex: ".$etiquetas['codigo_ecommerce'].""), 0, 0, 'L');

		$pdf->Ln(5);
		$pdf->Cell(190, 7, utf8_decode("Destinatário: ".$dados['destinatario'].""), 0, 0, 'L');

		$pdf->Ln(7);	
	}
}
/*--------------- // Objetos da plp----------------------------*/

} // Fecha else PLP Inválida
	
$pdf->Output('I','Correios PLP '.$plp.".pdf",true);
//$pdf->Output();


?>
