<!--cabeçalho-->
<header>
	<!--nav-->
    <nav class="navbar navbar-expand-sm navbar-ligth">
        <!--container-->
		<div class="container">
            <!--logo-->
			<div>
            	<img src="./assets/img/logo-farmaciaindiana.png" width="100"> 
			</div>
			<!--//logo-->
			
			<!--Itens da nav-->
            <div class="collapse navbar-collapse" id="nav-principal">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                    	<a href="index.php" class="nav-link">Em aberto</a>
                    </li>
                    <li class="nav-item">
                        <a href="plps_correios.php" class="nav-link">PLPs Correios</a>
                    </li>
                    <li class="nav-item">
                        <a href="plps_jadlog.php" class="nav-link">PLPs Jadlog</a>
                    </li>
                    <li class="nav-item">
                        <a href="tracking_front.php" class="nav-link">Enviar tracking</a>
                    </li>
                </ul>
                <?=$_SESSION['cd_usu'];?> - <?=$_SESSION['nm_usu']?>
                <a href="logout.php" class="btn btn-danger btn-sm ml-2">Sair</a>
            </div>
			<!--//Itens da nav--> 	
			
		</div>
		<!--//container-->
    </nav>
	<!--//nav-->
</header>
<!--//cabeçalho-->
