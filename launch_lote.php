<?php
require_once 'checkout_session.php';
include 'db_querys.php';
error_reporting(~E_ALL);

if (isset($_POST['id_plp'])) {
  $_SESSION['id_plp'] = $_POST['id_plp'];

} else {
  if(!isset($_SESSION['id_plp'])){
    header('Location: index.php');
  }

}

$conect = new DbConnection();
$dbQuerys = new DbQuerys;

$query = "exec p_log_ecom_lst_pend @so_pend = 0,@plp = {$_SESSION['id_plp']}";        
$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());

if (isset($_POST['code'])) {
	$retorno = $dbQuerys->confereEtiqueta($_SESSION['id_plp'], $_POST['code'], $_SESSION['cd_usu']);
}


?>
<!DOCTYPE html>
<html lang="pt-br" class="default-style">
  <head>
    <title>Conferência de Etiquetas</title>

    <?php require_once './assets/layout/head-config.html' ?>
  </head>

  <body class="background-color">
    <?php require_once './assets/layout/header.php'; ?>
    <!--Container-->
    <div class="container py-5">
        <?php if(($retorno != 'ok') and ($retorno != '')){ ?> 
          <p class='alert alert-danger font-weight-bold text-center' role='alert' style="font-size: 2rem">
            <?php echo utf8_encode($retorno); ?>
          </p>
        <?php } ?> 

      <div class="d-flex justify-content-center my-5">
        <h1 class="display-3 font-weight-bold font-color">Conferência de etiquetas</h1>
      </div>

      <!--row-->
      <div class="row my-4">
        <!--col-md-12-->
        <div class="col-md-12">
          <!--form-->
          <form action="#" method="post">
            <div class="form-group">
              <label for="formGroupExampleInput">Código: </label>
              <input
                inputCursor
                type="text"
                name="code"
                class="form-control"
                id="formGroupExampleInput"
                placeholder=""
                required
              />
            </div>
            <button hidden></button>
          </form>
          <!--form-->
        </div>
        <!--col-md-12-->
      </div>
      <!--row-->

      <!--row-->
      <div class="row">
        <!--col-md-12-->
        <div class="col-md-12">
			<div class="row">
			<div class="col-sm">
          		<h1 class="display-4">
			  		<a href="index.php"><img src="assets/img/back.png" width="50"></a>
			  		Status:
		  		</h1>
			</div>
			<div class="col-sm">
		  		<h1 class="display-4"><?php echo "Conferidos: ".$dbQuerys->countConferidos($_SESSION['id_plp']); ?></h1>
			</div>
			</div>
          <table class="table">
            <thead class="thead-primary">
                <tr>
                    <th scope="col">Orçamento</th>
                    <th scope="col">E-commerce</th>
                    <th scope="col">Transportadora</th>
                </tr>
            </thead>
            <tbody>
            <?php 
				if($conn){
					$query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));
					while($ret = sqlsrv_fetch_array($query)){ ?>
						<tr bgcolor='<?php echo $ret['cor_status']; ?>'>	
							<td><?php echo $ret['cd_orc']?></td>
							<td><?php echo $ret['codigo_ecommerce']?></td>
							<td><?php echo $ret['nm_transportadora']?></td>

						</tr>
				<?php	} // Fecha WHILE
				}else{
                    return die (print_r(sqlsrv_errors(),true));
                } // Fecha IF
			
			?>
			</tbody>
          </table>
        </div>
        <!--col-md-12-->
      </div>
      <!-- // row-->
    </div>
    <!-- // Container-->

    <?php require_once './assets/layout/footer.html'; ?>

    <!--scriptInput-->
    <script type="text/javascript">
      const input = document.querySelector('input[inputCursor]');
      const inputCompare = input.getAttributeNode('inputCursor');
      input.focus();

      input.addEventListener("blur", e => {
        const element = e.target;
        const attributeElement = element.getAttributeNode('inputCursor');

        if (attributeElement === inputCompare) {
          element.placeholder = "Clique aqui antes de bipar..";
        }
      });
    </script>

    <?php require_once './assets/layout/scripts.html'; ?>
  </body>
</html>
