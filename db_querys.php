<?php

include 'db_connection.php';
	
class DbQuerys
{
	
    public function autenticaUsuario($user, $password)
    {
        $conect = new DbConnection();
        
        $query = "exec p_autentica_sistemas_itec @user= ".$user.", @password= '".$password."'";
        		
		$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());
        
        if($conn){
            $query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));
            $query = sqlsrv_fetch_array($query);

            $returnQuery = $query;
        }else{
            return die (print_r(sqlsrv_errors(),true));
        }
        
        return $returnQuery;
    }      

/*------------------------------------------------------------------------------------------*/

	public function getTransportadora($cd_transportadora)
    {
        $conect = new DbConnection();
        
        $query = "select ds_emp_entrega from TELE_EMPRESA_ENTREGA where cd_emp_entrega = ".$cd_transportadora;
        
		$conn = sqlsrv_connect($conect->server, $conect->connectionIndianaProducao()());
        
        if($conn){
            $query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));
            while($transp = sqlsrv_fetch_array($query)){
                $returnQuery = $transp['ds_emp_entrega'];
            }
        }else{
            return die (print_r(sqlsrv_errors(),true));
        }
        
        return $returnQuery;
    }	
	
/*------------------------------------------------------------------------------------------*/
	
    public function getTranspAbertas() 
    {
        $conect = new DbConnection();
        
        if($conect->dev == "homolog"){
            $query = "select count(*) as qtd from TELE_EMPRESA_ENTREGA t 
					where t.ATIVO = 1
     				and t.COLETA_UNICA = 0
	 				and not t.CD_EMP_ENTREGA in (select cd_transportadora from IND_Retaguarda_DEV..log_ecommerce_conf_plp where dthr_fech is null)";

        }else{
            $query = "select count(*) as qtd from TELE_EMPRESA_ENTREGA t 
					where t.ATIVO = 1
     				and t.COLETA_UNICA = 0
	 				and not t.CD_EMP_ENTREGA in (select cd_transportadora from IND_Retaguarda..log_ecommerce_conf_plp where dthr_fech is null)";

        }
        
		$conn = sqlsrv_connect($conect->server, $conect->connectionIndianaProducao());

        if($conn){
            $query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));
            $query = sqlsrv_fetch_array($query);

            $returnQuery = $query;
        }else{
            return die (print_r(sqlsrv_errors(),true));
        }
        
        return $returnQuery;
    }

/*------------------------------------------------------------------------------------------*/	
	
	public function abreLote($usu_conf,$cd_transportadora)
	{
        $conect = new DbConnection();
        
        $query = "exec p_log_ecom_grv_lote @oper = 'A',@usu_conf = ".$usu_conf.", @cd_transportadora = '$cd_transportadora'";
        
		$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());
        
        if($conn){
            $query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));
			
			while ($ret = sqlsrv_fetch_array($query)){
				$returnQuery = utf8_encode($ret['retorno']);
			}
        }else{
            return die (print_r(sqlsrv_errors(),true));
        }
        
        return $returnQuery;
    }      

/*------------------------------------------------------------------------------------------*/	
	
	public function fechaLote($id_plp)
	{
        $conect = new DbConnection();
        
        $query = "exec p_log_ecom_grv_lote @oper = 'F', @id_plp = ".$id_plp."";
        
		$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());
        
        if($conn){
            $query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));
			
			while ($ret = sqlsrv_fetch_array($query)){
				$returnQuery = utf8_encode($ret['retorno']);
			}
        }else{
            return die (print_r(sqlsrv_errors(),true));
        }
        
        return $returnQuery;
    }      

/*------------------------------------------------------------------------------------------*/	

	public function confereEtiqueta($id_plp,$cd_rastreio, $user){
		
        $conect = new DbConnection();

        $query = "exec p_log_ecom_lnc_etiq @id_plp = ".$id_plp." ,@cd_rastreio = '".$cd_rastreio."' ,@cd_usu = ".$user."";
        
		$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());
        
        $query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));
            
		while ($ret = sqlsrv_fetch_array($query)){
			$returnQuery = utf8_encode($ret['retorno']);
		}
		
        return $returnQuery;
		
	}
	
/*------------------------------------------------------------------------------------------*/	

	public function getCdPlpCorreios($id_plp){
		
        $conect = new DbConnection();

        $query = "select cd_plp from log_ecommerce_conf_plp where id_plp = ".$id_plp." and cd_transportadora = 16";
        
		$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());
        
        if($conn){
            $query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));
            
			while ($ret = sqlsrv_fetch_array($query)){
				$returnQuery = $ret['cd_plp'];
			}
        }else{
            return die (print_r(sqlsrv_errors(),true));
        }
        
        return $returnQuery;
		
	}
	
	
/*------------------------------------------------------------------------------------------*/	

	public function countConferidos($id_plp){
		
        $conect = new DbConnection();

        $query = "select count(*) as qtd from log_ecommerce_conf_etiquetas where id_plp = ".$id_plp."";
        
		$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());
        
        if($conn){
            $query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));
            
			while ($ret = sqlsrv_fetch_array($query)){
				$returnQuery = $ret['qtd'];
			}
        }else{
            return die (print_r(sqlsrv_errors(),true));
        }
        
        return $returnQuery;
		
	}
	
/*------------------------------------------------------------------------------------------*/	

	public function getDadosTracking($cd_orc){
		
        $conect = new DbConnection();

        $query = "exec p_log_ecom_lst_track @lista = 0,@cd_orc = ".$cd_orc."";
        
		$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());
        
        if($conn){
            $query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));
            
			while ($ret = sqlsrv_fetch_array($query)){
				$returnQuery['cd_rastreio'] 		= $ret['cd_rastreio'];
				$returnQuery['courier'] 			= $ret['courier'];
				$returnQuery['dispatchedDate'] 		= $ret['dispatchedDate'];
				$returnQuery['cd_orc'] 				= $ret['cd_orc'];
				$returnQuery['codigo_ecommerce'] 	= $ret['codigo_ecommerce'];
				$returnQuery['invoice'] 			= $ret['invoice'];
			}
        }else{
            return die (print_r(sqlsrv_errors(),true));
        }
        
        return $returnQuery;
		
	}
	
/*------------------------------------------------------------------------------------------*/	

	public function updateRetVTex($cd_orc,$retVTex){
		
        $conect = new DbConnection();

        $query = "update log_ecommerce_conf_etiquetas set ret_vtex = '".$retVTex."' where cd_orc = ".$cd_orc."";
        
		$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());
        
        if($conn){
            $query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));
            
			$ret = sqlsrv_fetch_array($query);
			
        }else{
            return die (print_r(sqlsrv_errors(),true));
        }
		
	}
	
/*------------------------------------------------------------------------------------------*/		
	
} // Fecha Classe
	
    
    

