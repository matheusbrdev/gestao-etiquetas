<?php 
// ini_set('xdebug.var_display_max_depth', '10');
// ini_set('xdebug.var_display_max_children', '256');
// ini_set('xdebug.var_display_max_data', '1024');

require_once 'checkout_session.php';
include 'soap_operations.php';


$conect = new DbConnection();
$soap = new soapOperations();

$plp = $_GET['plp'];
$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());
$query = "exec p_log_ecom_lst_pend @so_pend = 2,@plp = ".$plp."";        
$query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));


/*-----------------------SOAP---------------------*/

$etiquetas = array();
$objetos = "";


while($result = sqlsrv_fetch_array($query)){
	$urlEncodeCdRastreio = urlencode($result['cd_rastreio']);
	$cDRastreio = str_replace('+', '', $urlEncodeCdRastreio);
	$etiquetas[] = $cDRastreio;
	$objeto = $soap->geraObjetoPostal($result['cd_orc']);
	$objetos .= $objeto;
}

	
/*-----------------------// SOAP---------------------*/

?>
<html lang="pt-br" class="default-style">
  <head>
    <title>Geração de PLP nos Correios</title>

    <?php require_once './assets/layout/head-config.html' ?>
    <style>
        #divMessage {
            height: 90%;
        }
    </style>
  </head>

  <body class="background-color">
    <?php require_once './assets/layout/header.php'; ?>
    <div class="container">
      <div id="divMessage" class="d-flex flex-column justify-content-center align-items-start">
        <?php if($soap->fecharPlp($plp,$objetos,$etiquetas) == 1): ?>
            <div class="d-flex">
                <a href='index.php' class='btn btn-success mt-5 text-white'>
                    Voltar
                </a>
                <a class="btn btn-primary text-white align-self-lg-end ml-2" href="tracking_front.php">Enviar tracking</a>
            </div>
        <?php endif ?>
      </div>
    </div>

	
	<?php require_once './assets/layout/footer.html'; ?>    
    <?php require_once './assets/layout/scripts.html'; ?>
  </body>
</html>
