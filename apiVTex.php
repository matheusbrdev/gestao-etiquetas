<?php

class apiVTex{
	
	function enviaTracking($cd_rastreio,$courier,$dataEnvio,$cdOrc,$codigoEcommerce,$invoice){
		
    	// Endpoint
		$url = "http://farmaciaindiana.vtexcommercestable.com.br/api/oms/pvt/orders/".$codigoEcommerce."/invoice/".$invoice;

		//Inicialização do curl
    	$iniciar = curl_init();
		
		// Criação do array dos Headers
    	$customHeaders = array(
			'X-VTEX-API-AppKey: vtexappkey-farmaciaindiana-VPEIPE',
			'X-VTEX-API-AppToken: KDGPNUHVEAZXHTJDYGXHFCTMDJCNMMZKRQSMDAJFLPPHSIICTJJTPYPHJUMDRTJSBPFRBXAYWRVQNMTUBZUCCFVTAKCJNFNSGWFDDQBVJLWXDZTXBSCSYGQVUMRQVQKQ',
			'Content-Type: application/json'
    	);

        curl_setopt($iniciar, CURLOPT_URL, $url);

		// Passando os headers para chamada
		curl_setopt($iniciar, CURLOPT_HTTPHEADER, $customHeaders);

		// Verificação da conexão SSH
		curl_setopt($iniciar, CURLOPT_SSL_VERIFYPEER, false); 

		// Converte o arquivo para não deixar em uma única string
		curl_setopt($iniciar,CURLOPT_RETURNTRANSFER,true);
		
		// Método do consumo da API
        curl_setopt($iniciar, CURLOPT_CUSTOMREQUEST, 'PATCH');
		
		if($courier == 'Jadlog')
			$linkRastreio = "https://www.jadlog.com.br/tracking";
		else
			$linkRastreio = "https://www.linkcorreios.com.br/?id=".$cd_rastreio."";
		
		// Montadem dos dados para serem enviados
		$json = "{
			'trackingNumber':'".$cd_rastreio."',
    		'trackingUrl':'".$linkRastreio."',
    		'courier':'".$courier."',
  			'dispatchedDate':'".$dataEnvio."'
		}";
		
		// Envio dos dados
		curl_setopt($iniciar,CURLOPT_POSTFIELDS,$json);
		
		// Execução
		curl_exec($iniciar);

		// Decodifica o resultado recebido da API em JSON
		$resultado = json_decode(curl_exec($iniciar));

		$codRetorno = curl_getinfo($iniciar, CURLINFO_HTTP_CODE);
		
		if(($codRetorno == 200) or ($codRetorno == '200')){
			$resultado = $codRetorno;
		}
		
		curl_close($iniciar);
		return $resultado;
		}

/*------------------------------------------------------------------------------------------*/	
	
	function getStatusPedidoVTex($codigoEcommerce){
		
    	// Endpoint
		$url = "https://farmaciaindiana.vtexcommercestable.com.br/api/oms/pvt/orders/".$codigoEcommerce;

		//Inicialização do curl
    	$iniciar = curl_init();
		
		// Criação do array dos Headers
    	$customHeaders = array(
			'X-VTEX-API-AppKey: vtexappkey-farmaciaindiana-VPEIPE',
			'X-VTEX-API-AppToken: KDGPNUHVEAZXHTJDYGXHFCTMDJCNMMZKRQSMDAJFLPPHSIICTJJTPYPHJUMDRTJSBPFRBXAYWRVQNMTUBZUCCFVTAKCJNFNSGWFDDQBVJLWXDZTXBSCSYGQVUMRQVQKQ',
			'Content-Type: application/json'
    	);

        curl_setopt($iniciar, CURLOPT_URL, $url);

		// Passando os headers para chamada
		curl_setopt($iniciar, CURLOPT_HTTPHEADER, $customHeaders);

		// Verificação da conexão SSH
		curl_setopt($iniciar, CURLOPT_SSL_VERIFYPEER, false); 

		// Converte o arquivo para não deixar em uma única string
		curl_setopt($iniciar,CURLOPT_RETURNTRANSFER,true);
		
		// Método do consumo da API
        curl_setopt($iniciar, CURLOPT_CUSTOMREQUEST, 'GET');
						
		// Execução
		curl_exec($iniciar);

		// Decodifica o resultado recebido da API em JSON
		$resultado = json_decode(curl_exec($iniciar));
		
		curl_close($iniciar);
		return $resultado;
		}



}

?>