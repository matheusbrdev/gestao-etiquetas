<?php
ini_set('xdebug.var_display_max_depth', '10');
ini_set('xdebug.var_display_max_children', '256');
ini_set('xdebug.var_display_max_data', '9000');

include 'db_querys.php';


class soapOperations
{
	protected $idContrato;
	protected $idCartaoPostagem;
	protected $usuario;
	protected $senha;

    function __construct() {
		
	
		$this->idContrato 			= "9912402057";
		$this->idCartaoPostagem		= "0072511117";
		$this->usuario 				= "25102146000179";
		$this->senha 				= "c31rix";

		$soapclient = new SoapClient('https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl');
		$params = array(
			"idContrato" 		=> $this->idContrato,
			"idCartaoPostagem" 	=> $this->idCartaoPostagem,
			"usuario"			=> $this->usuario,
			"senha" 			=> $this->senha
		);

		//$response = $soapclient->buscaCliente($params);

		$nome = urlencode($response->return->nome);
		$nome = str_replace('+++', '', $nome);
		$nome = urldecode(str_replace('+', ' ', $nome));

		$this->codigoDiretoria 		= "20";
		$this->codigoAdministrativo = "16248872";
		$this->codigoCliente 		= "4087480";
		$this->nome 				= "DROGARIA DRUGSTORE E FARMACIA INDIANA";
		$this->cnpj 				= "25102146000179";
		
		
		// Dados devem ser preenchidos de acordo com o retorno do buscaCliente
		/*
		$this->codigoDiretoria 		= $this->retiraEspaco($response->return->contratos->codigoDiretoria);
		$this->codigoAdministrativo = "16248872";
		$this->codigoCliente 		= $this->retiraEspaco($response->return->contratos->codigoCliente);
		$this->nome 				= $nome;
		$this->cnpj 				= $this->retiraEspaco($response->return->cnpj);
       	*/
	}
	
/*------------------------------------------------------------------------------------------*/
   
	private function retiraEspaco($value) {
		$value = urlencode($value);
		
		$value = urldecode(str_replace('+', '', $value));
		return $value;
   }

/*------------------------------------------------------------------------------------------*/
    
    public function gerarXMLCabecalho(){
		$xmlCabecalho = "<?xml version='1.0' encoding='ISO-8859-1' ?>
<correioslog>
	<tipo_arquivo>Postagem</tipo_arquivo>
	<versao_arquivo>2.3</versao_arquivo>
	<plp>
		<id_plp />
		<valor_global />
		<mcu_unidade_postagem/>
		<nome_unidade_postagem/>
		<cartao_postagem>0067599079</cartao_postagem>
	</plp>
	<remetente>
		<numero_contrato>9912402057</numero_contrato>
		<numero_diretoria>".$this->codigoDiretoria."</numero_diretoria>
		<codigo_administrativo>16248872</codigo_administrativo>
		<nome_remetente>
			<![CDATA[".substr($this->nome,0,50)."]]>
		</nome_remetente>
		<logradouro_remetente>
			<![CDATA[Rua Coronel Mario Cordeiro]]>
		</logradouro_remetente>
		<numero_remetente>
			<![CDATA[982]]>
		</numero_remetente>
		<complemento_remetente />
		<bairro_remetente>
			<![CDATA[Jardim Serra Verde]]>
		</bairro_remetente>
		<cep_remetente>
			<![CDATA[39801457]]>
		</cep_remetente>
		<cidade_remetente>
			<![CDATA[Teofilo Otoni]]>
		</cidade_remetente>
		<uf_remetente>MG</uf_remetente>
		<telefone_remetente>
			<![CDATA[3335291300]]>
		</telefone_remetente>
		<fax_remetente />
		<email_remetente>
			<![CDATA[sac@farmaciaindiana.com.br]]>
		</email_remetente>
		<cpf_cnpj_remetente>40606402000159</cpf_cnpj_remetente>
		<celular_remetente/>
		<ciencia_conteudo_proibido>S</ciencia_conteudo_proibido>
	</remetente>
	<forma_pagamento />";
		    //".$this->idContrato."
          //".$this->codigoAdministrativo."
        return $xmlCabecalho;
	}

/*------------------------------------------------------------------------------------------*/
	
    public function geraObjetoPostal($cd_orc){
		$conect = new DbConnection();
		$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());

		$query = "exec p_log_ecom_obj_post_correio @orc = ".$cd_orc."";        
		$query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));

		while($result = sqlsrv_fetch_array($query)){
		$objetoPostal = "
        <objeto_postal>
	<numero_etiqueta>".$result['cd_rastreio_correio_comp']."</numero_etiqueta>
	<codigo_objeto_cliente/>
	<codigo_servico_postagem>".$result['cd_servico']."</codigo_servico_postagem>
	<cubagem>0,00</cubagem>
	<peso>2500</peso>
	<rt1/>
	<rt2/>
	<restricao_anac/>
	<destinatario>
		<nome_destinatario>
			<![CDATA[".utf8_encode($result['rz_cli'])."]]>
		</nome_destinatario>
		<telefone_destinatario>
			<![CDATA[".substr($result['tel'],0,10)."]]>
		</telefone_destinatario>
		<celular_destinatario>
			<![CDATA[".substr($result['cel'],0,10)."]]>
		</celular_destinatario>
		<email_destinatario>
			<![CDATA[".trim(substr($result['email'],0,50))."]]>
		</email_destinatario>
		<logradouro_destinatario>

			<![CDATA[".utf8_encode($result['logra'])."]]>
		</logradouro_destinatario>
		<complemento_destinatario>
			<![CDATA[".utf8_encode(substr($result['complemento'],0,30))."]]>
		</complemento_destinatario>
		<numero_end_destinatario>
			<![CDATA[".$result['numero']."]]>
		</numero_end_destinatario>
		<cpf_cnpj_destinatario>".$result['cgc_cpf']."</cpf_cnpj_destinatario>
	</destinatario>
	<nacional>
		<bairro_destinatario>
			<![CDATA[".utf8_encode(substr($result['bairro'],0,30))."]]>
		</bairro_destinatario>
		<cidade_destinatario>
			<![CDATA[".utf8_encode(substr($result['ds_cid'],0,30))."]]>
		</cidade_destinatario>
		<uf_destinatario>".$result['uf']."</uf_destinatario>
		<cep_destinatario>
			<![CDATA[".$result['cep']."]]>
		</cep_destinatario>
		<codigo_usuario_postal/>
		<centro_custo_cliente/>
		<numero_nota_fiscal>".$result['nf_nf']."</numero_nota_fiscal>
		<serie_nota_fiscal/>
		<valor_nota_fiscal/>
		<natureza_nota_fiscal/>
		<descricao_objeto>
			<![CDATA[".$result['cd_rastreio_correio_comp']."]]>
		</descricao_objeto>
		<valor_a_cobrar>0,0</valor_a_cobrar>
	</nacional>
	<servico_adicional>
		<codigo_servico_adicional>025</codigo_servico_adicional>
		<valor_declarado></valor_declarado>
	</servico_adicional>
	<dimensao_objeto>
		<tipo_objeto>002</tipo_objeto>
		<dimensao_altura>20,00</dimensao_altura>
		<dimensao_largura>15,00</dimensao_largura>
		<dimensao_comprimento>20,00</dimensao_comprimento>
		<dimensao_diametro>0,00</dimensao_diametro>
	</dimensao_objeto>
	<data_postagem_sara/>
	<status_processamento>0</status_processamento>
	<numero_comprovante_postagem/>
	<valor_cobrado/>
</objeto_postal>
";
		}
        return $objetoPostal;
	}

/*------------------------------------------------------------------------------------------*/
	
    public function fecharPlp($idPlp, $xmlObjetos, $listaEtiquetas){
		
		$soapclient = new SoapClient('https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl');

		$params = [
			'xml' => $this->gerarXMLCabecalho().$xmlObjetos."</correioslog>",
			'idPlpCliente' => $idPlp,
			'cartaoPostagem' => $this->idCartaoPostagem,
			'listaEtiquetas' => $listaEtiquetas,
			'usuario' =>  $this->usuario,
			'senha' => $this->senha 
		];
        $returnStatus = 0;
        $returnPlp = 0;
		
        try {
            $returnXml = $soapclient->fechaPlpVariosServicos($params);
            echo "<h1 class='display-3 text-success'>Vaaleu!</h1>
                <p>PLP fechada com sucesso :)</p>
                <p class='font-weight-bold'>Código PLP: ".$returnXml->return."</p>
            ";
            $returnStatus = 1;
            $returnPlp = 1;

        }catch (Exception $e) {
           echo "
                <h1 class='display-3 text-danger'>Ops!</h1>
                <p>Não foi possível completar fechamento:<br> {$e->getMessage()} </p>
                <br>
                 <a href='index.php' class='btn btn-danger mt-5 text-white'>
                    Voltar
                 </a>
           ";
        }

        if ($returnStatus == 1) {
            $conect = new DbConnection();
            $conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());

            $query = "update log_ecommerce_conf_plp set cd_plp = ".$returnXml->return." where id_plp = ".$idPlp."";
            $query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));
        }

        return $returnPlp;
	}
}

?>