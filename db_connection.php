<?php

class DbConnection
{
  	
	public $dev;
	public $server;
	
	function __construct(){
		$this->dev = "prod"; // "prod" ou "homolog"
		
		if($this->dev == "homolog")
			$this->server = "192.168.248.210";
		else
			$this->server = "192.168.1.210";
	}

    
    public function connectionINDRetaguarda()
    {
        if($this->dev == "homolog"){
			$connectionInfo = array(
				"Database" => "IND_Retaguarda_DEV",
				"UID" => "appweb",
				"PWD" => "4ppw3bd3v$$"
			);
		}else{
			$connectionInfo = array (
				"Database" => "IND_Retaguarda",
				"UID" => "appweb",
				"PWD" => "4ppw3bd3v$$"
			);
		}
		
        return $connectionInfo;
    }
    
    public function connectionIndianaProducao()
    {
        if($this->dev == "homolog"){
			$connectionInfo = array(
				"Database" => "IndianaProducao_DEV",
				"UID" => "appweb",
				"PWD" => "4ppw3bd3v$$"
			);
		}else{
			$connectionInfo = array(
				"Database" => "IndianaProducao",
				"UID" => "appweb",
				"PWD" => "4ppw3bd3v$$"
			);
		}
        
        return $connectionInfo;
    }

}
