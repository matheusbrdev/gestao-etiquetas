<?php 

require_once 'checkout_session.php';
include 'db_querys.php';

$conect = new DbConnection();
$dbQuerys = new DbQuerys;

$plp = $_GET['plp'];
$query = "exec p_log_ecom_lst_pend @so_pend = 2,@plp = '".$plp."'";
$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());

?>
<!DOCTYPE html>
<html lang="pt-br" class="default-style">
  <head>
    <title>Etiquetas Correios</title>

    <?php require_once './assets/layout/head-config.html' ?>

    <!--Datatables-->
    <link
      rel="stylesheet"
      href="assets/css/jquery.dataTables.min.css"
    >
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>
    <script src="./assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        $.noConflict();
        $('#dataTables').DataTable();
      } );
    </script>

  </head>

  <body class="background-color">
    <?php require_once './assets/layout/header.php'; ?>
    <!--Container-->
    <div class="container py-3">
        <?php if ($_SESSION['retorno']){
          echo "<p class='alert alert-warning font-weight-bold text-center' role='alert'>{$_SESSION['retorno']}</p>";
          $_SESSION['retorno'] = '';
        } 
        ?>  
      <div class="d-flex justify-content-center my-5">
        <h1 class="display-3 font-weight-bold font-color">
          PLPs Correios
        </h1>
      </div>

      <!--row-->
      <div class="row">
        <!--col-md-12-->
        <div class="col-md-12">
          <div class="row">
            <div class="col-6 d-flex justify-content-start">
				<h1 class="display-4">
					<a href="plps_correios.php"><img src="assets/img/back.png" width="50"></a>Etiquetas:
				</h1>
            </div>
          </div>
          <table class="table" id="dataTables">
            <thead class="thead-primary">
              <tr>
                <th scope="col">Orçamento</th>
                <th scope="col">Cód. E-commerce</th>
                <th scope="col">Cód. Rastreio</th>
                <th scope="col">Transportadora</th>
              </tr>
            </thead>
            <tbody><?php 
                if($conn){
                    $query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));    
                    while($result = sqlsrv_fetch_array($query)){ ?>
						<tr>
							<td><?php echo $result['cd_orc']; ?></td>
							<td><?php echo $result['codigo_ecommerce']; ?></td>
							<td><?php echo $result['cd_rastreio']; ?></td>
							<td><?php echo $result['nm_transportadora']; ?></td>							
						</tr>

			<?php    } // Fecha while fetch_array
                } // Fecha IF $conn
                else{
                    return die (print_r(sqlsrv_errors(),true));
                }
                ?>
              
            </tbody>
          </table>
        </div>
        <!--col-md-12-->
      </div>
      <!-- // row-->
    </div>
    <!-- // Container-->
      
    <?php require_once './assets/layout/footer.html'; ?>    
    <?php require_once './assets/layout/scripts.html'; ?>
  </body>
</html>
