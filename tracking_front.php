<?php
require_once 'checkout_session.php';
include 'db_connection.php';

$conect = new DbConnection();

$query = "exec p_log_ecom_lst_track @lista = 1";
$conn = sqlsrv_connect($conect->server, $conect->connectionINDRetaguarda());
$query = sqlsrv_query($conn, $query) or die(print_r(sqlsrv_errors(), true));


?>
<!DOCTYPE html>
<html lang="pt-br" class="default-style">
<head>
    <title>Envio de tracking</title>

    <?php require_once './assets/layout/head-config.html' ?>
</head>

<body id="bodyIndex" class="background-color">
<?php require_once './assets/layout/header.php'; ?>
<!--Container-->
<div class="container py-3">
    <div class="d-flex justify-content-center my-5">
        <h1 class="display-3 font-weight-bold font-color">
            Enviar Tracking
        </h1>
    </div>

    <!--row -->
    <form action="tracking_retorno.php" method="post">
        <div class="row">
            <!--col-md-12-->
            <div class="col-md-12">
                <div class="row">
                    <div class="col-6 d-flex justify-content-start">
                        <h1 class="display-4">Itens:</h1>
                    </div>
					<div class="col-6 d-flex justify-content-end align-items-start">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >
                            Enviar
                        </button>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Confirmação de envio de Tracking</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Nenhum item selecionado
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                    <button type="submit"  id="buttonModalSend" class="btn btn-primary" disabled>
                                        Enviar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- / MODAL -->
                </div>

                <table class="table">
                    <thead class="thead-primary">
                        <tr>
                            <th>
                                <input type="checkbox" class="mr-1" id="allSelect"/>
                            </th>
							<th scope="col">Orçamento</th>
							<th scope="col">Pedido VTex</th>
							<th scope="col">PLP Indiana</th>
                            <th scope="col">PLP Transportadora</th>
                            <th scope="col">Cod. Rastreio</th>
                            <th scope="col">Serviço</th>
                            <th scope="col">Retorno VTex</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            while($ret = sqlsrv_fetch_array($query)){ ?>
                                <tr>
                                    <td><input type="checkbox" name="check[]" value=<?php echo $ret['cd_orc']; ?> /></td>
                                    <td><?php echo $ret['cd_orc']; ?></td>
                                    <td><?php echo $ret['codigo_ecommerce']; ?></td>
                                    <td><?php echo $ret['id_plp']; ?></td>
                                    <td><?php echo $ret['cd_plp']; ?></td>
                                    <td><?php echo $ret['cd_rastreio']; ?></td>
                                    <td><?php echo $ret['ds_tipo_entrega_empresa']; ?></td>
                                    <td><?php echo $ret['ret_vtex']; ?></td>
                                </tr>

                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <!--col-md-12-->
        </div>
    </form>
    <!-- // row-->
</div>
<!-- // Container-->

<script>
    const checkAllSelect = document.querySelector("input#allSelect");
    const allCheckbox = document.querySelectorAll("tbody input[type='checkbox']");
    const divModalBody = document.querySelector('div.modal-body');
    const buttonModalSend = document.querySelector('button#buttonModalSend');
    let elementsCheckedAlone = 0;

    checkAllSelect.addEventListener('click', e => {
        if (checkAllSelect.checked === true) {
            allCheckbox.forEach(e => {
                e.checked = true;
                elementsCheckedAlone++;
            });

            divModalBody.innerHTML = `<h5>Enviar:  ${elementsCheckedAlone} pedidos</h5>`;
            buttonModalSend.removeAttribute('disabled')
        } else {
            allCheckbox.forEach(e => {
                e.checked = false;
                elementsCheckedAlone--;
            });
            if (elementsCheckedAlone <= 0) {
                elementsCheckedAlone = 0;
                buttonModalSend.setAttribute('disabled','')
            }
            divModalBody.innerHTML = `<h5>Enviar:  ${elementsCheckedAlone} pedidos</h5>`;
        }
    });


    allCheckbox.forEach( e => {
        e.onclick = function () {
            if (e.checked === true){
                elementsCheckedAlone++;
                divModalBody.innerHTML = `<h5>Enviar:  ${elementsCheckedAlone} pedido(s)</h5>`;
                buttonModalSend.removeAttribute('disabled')
            } else {
                elementsCheckedAlone--;
                if (elementsCheckedAlone <= 0) {
                    elementsCheckedAlone = 0;
                    buttonModalSend.setAttribute('disabled','')
                }
                divModalBody.innerHTML = `<h5>Enviar:  ${elementsCheckedAlone} pedido(s)</h5>`;
            }
        };
    });
</script>
<?php require_once './assets/layout/footer.html'; ?>
<?php require_once './assets/layout/scripts.html'; ?>
</body>
</html>
